export const allOrders = `
query {
  allOrders {
    data {
      _id
      customer {
        _id
        name
        email
        address {
          line1
          line2
          city
          state
          postal_code
        }
      }
      items {
        name
        quantity
        total
      }
    }
  }
}`;
