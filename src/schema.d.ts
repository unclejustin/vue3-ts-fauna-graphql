export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  Date: any;
  Time: any;
  /** The `Long` scalar type represents non-fractional signed whole numeric values. Long can represent values between -(2^63) and 2^63 - 1. */
  Long: any;
};







/** 'Address' input values */
export type AddressInput = {
  line1: Scalars['String'];
  line2?: Maybe<Scalars['String']>;
  city: Scalars['String'];
  state: Scalars['String'];
  postal_code: Scalars['String'];
};

/** 'Customer' input values */
export type CustomerInput = {
  name: Scalars['String'];
  email?: Maybe<Scalars['String']>;
  address?: Maybe<AddressInput>;
  orders?: Maybe<CustomerOrdersRelation>;
};

/** Allow manipulating the relationship between the types 'Customer' and 'Order'. */
export type CustomerOrdersRelation = {
  /** Create one or more documents of type 'Order' and associate them with the current document. */
  create?: Maybe<Array<Maybe<OrderInput>>>;
  /** Connect one or more documents of type 'Order' with the current document using their IDs. */
  connect?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Disconnect the given documents of type 'Order' from the current document using their IDs. */
  disconnect?: Maybe<Array<Maybe<Scalars['ID']>>>;
};


/** 'LineItem' input values */
export type LineItemInput = {
  name: Scalars['String'];
  total: Scalars['Int'];
  quantity: Scalars['Int'];
};

export type Mutation = {
  __typename?: 'Mutation';
  /** Create a new document in the collection of 'Order' */
  createOrder: Order;
  /** Delete an existing document in the collection of 'Order' */
  deleteOrder?: Maybe<Order>;
  /** Update an existing document in the collection of 'Order' */
  updateOrder?: Maybe<Order>;
  /** Update an existing document in the collection of 'Customer' */
  updateCustomer?: Maybe<Customer>;
  /** Delete an existing document in the collection of 'Customer' */
  deleteCustomer?: Maybe<Customer>;
  /** Create a new document in the collection of 'Customer' */
  createCustomer: Customer;
};


export type MutationCreateOrderArgs = {
  data: OrderInput;
};


export type MutationDeleteOrderArgs = {
  id: Scalars['ID'];
};


export type MutationUpdateOrderArgs = {
  id: Scalars['ID'];
  data: OrderInput;
};


export type MutationUpdateCustomerArgs = {
  id: Scalars['ID'];
  data: CustomerInput;
};


export type MutationDeleteCustomerArgs = {
  id: Scalars['ID'];
};


export type MutationCreateCustomerArgs = {
  data: CustomerInput;
};

/** Allow manipulating the relationship between the types 'Order' and 'Customer' using the field 'Order.customer'. */
export type OrderCustomerRelation = {
  /** Create a document of type 'Customer' and associate it with the current document. */
  create?: Maybe<CustomerInput>;
  /** Connect a document of type 'Customer' with the current document using its ID. */
  connect?: Maybe<Scalars['ID']>;
};

/** 'Order' input values */
export type OrderInput = {
  items: Array<LineItemInput>;
  customer?: Maybe<OrderCustomerRelation>;
};


export type Address = {
  __typename?: 'Address';
  city: Scalars['String'];
  state: Scalars['String'];
  line1: Scalars['String'];
  postal_code: Scalars['String'];
  line2?: Maybe<Scalars['String']>;
};

export type Customer = {
  __typename?: 'Customer';
  name: Scalars['String'];
  email?: Maybe<Scalars['String']>;
  /** The document's ID. */
  _id: Scalars['ID'];
  orders: OrderPage;
  address?: Maybe<Address>;
  /** The document's timestamp. */
  _ts: Scalars['Long'];
};


export type CustomerOrdersArgs = {
  _size?: Maybe<Scalars['Int']>;
  _cursor?: Maybe<Scalars['String']>;
};

export type LineItem = {
  __typename?: 'LineItem';
  name: Scalars['String'];
  total: Scalars['Int'];
  quantity: Scalars['Int'];
};

export type Order = {
  __typename?: 'Order';
  /** The document's ID. */
  _id: Scalars['ID'];
  /** The document's timestamp. */
  _ts: Scalars['Long'];
  items: Array<LineItem>;
  customer: Customer;
};

/** The pagination object for elements of type 'Order'. */
export type OrderPage = {
  __typename?: 'OrderPage';
  /** The elements of type 'Order' in this page. */
  data: Array<Maybe<Order>>;
  /** A cursor for elements coming after the current page. */
  after?: Maybe<Scalars['String']>;
  /** A cursor for elements coming before the current page. */
  before?: Maybe<Scalars['String']>;
};

export type Query = {
  __typename?: 'Query';
  /** Find a document from the collection of 'Customer' by its id. */
  findCustomerByID?: Maybe<Customer>;
  /** Find a document from the collection of 'Order' by its id. */
  findOrderByID?: Maybe<Order>;
  allOrders: OrderPage;
};


export type QueryFindCustomerByIdArgs = {
  id: Scalars['ID'];
};


export type QueryFindOrderByIdArgs = {
  id: Scalars['ID'];
};


export type QueryAllOrdersArgs = {
  _size?: Maybe<Scalars['Int']>;
  _cursor?: Maybe<Scalars['String']>;
};

